import random
import matplotlib.pyplot as plt
import numpy as np

r = []

for _ in range(1000):
    s = 0
    p = 0
    while p < 25:
        p += random.randint(1, 6)
        if p == 1:
            p = 12
        elif p == 13:
            p = 22
        elif p == 14:
            p = 3
        elif p == 20:
            p = 8
        s += 1
    r.append(s)

print(f'Shortest game duration: {min(r):4d}')
print(f'Mean game duration    : {np.mean(r):6.1f} ± {np.std(r):.1f}')
print(f'Longest game duration : {max(r):4d}')

hv, hb = np.histogram(r, bins=np.arange(0, max(r)))
plt.figure(figsize=(8, 3))
plt.step(hb[:-1], hv)
plt.show()
