"""
Chutes & Ladders Game

- v1: Board implemented as class
- v2: Game implemented as class
- v3: Experiment implemented as class
- v4: Multiple players and configurable board
"""

import random
import matplotlib.pyplot as plt
import numpy as np


class Board:
    def __init__(self, goal=25, chutes_and_ladders=None):
        self.goal = goal
        if chutes_and_ladders is None:
            self.chutes_and_ladders = {1: 12, 13: 22, 14: 3, 20: 8}
        else:
            self.chutes_and_ladders = chutes_and_ladders

    def goal_reached(self, position):
        return position >= self.goal

    def adjust_position(self, position):
        if position in board.chutes_and_ladders:
            return board.chutes_and_ladders[position]
        else:
            return position


class Player:
    def __init__(self, board):
        self.board = board
        self.position = 0
        self.num_moves = 0

    def make_move(self):
        self.position += random.randint(1, 6)
        self.position = self.board.adjust_position(self.position)
        self.num_moves += 1

    def goal_reached(self):
        return self.board.goal_reached(self.position)


class Game:
    def __init__(self, board, num_players):
        self.board = board
        self.num_players = num_players

    def play(self):
        players = [Player(self.board) for _ in range(self.num_players)]
        while not any(player.goal_reached() for player in players):
            for player in players:
                player.make_move()

        return player.num_moves


class Experiment:
    def __init__(self, num_games, seed, board, num_players):
        random.seed(seed)
        self.board = board
        self.num_games = num_games
        self.num_players = num_players

    def execute(self):
        return [Game(self.board, self.num_players).play() for _ in range(self.num_games)]


if __name__ == "__main__":

    board = Board(50, {40: 3})

    exper = Experiment(1000, 1710, board, 2)
    durations = exper.execute()

    print(f'Shortest game duration: {min(durations):4d}')
    print(f'Mean game duration    : {np.mean(durations):6.1f} ± {np.std(durations):.1f}')
    print(f'Longest game duration : {max(durations):4d}')

    hv, hb = np.histogram(durations, bins=np.arange(0, max(durations)))
    plt.figure(figsize=(8, 3))
    plt.step(hb[:-1], hv)
    plt.show()
