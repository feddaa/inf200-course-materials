"""
Chutes & Ladders Game

- v1: Board implemented as class
- v2: Game implemented as class
"""

import random
import matplotlib.pyplot as plt
import numpy as np


class Board:
    def __init__(self):
        self.goal = 25
        self.chutes_and_ladders = {1: 12, 13: 22, 14: 3, 20: 8}

    def goal_reached(self, position):
        return position >= self.goal

    def adjust_position(self, position):
        if position in board.chutes_and_ladders:
            return board.chutes_and_ladders[position]
        else:
            return position


class Player:
    def __init__(self, board):
        self.board = board
        self.position = 0
        self.num_moves = 0

    def make_move(self):
        self.position += random.randint(1, 6)
        self.position = self.board.adjust_position(self.position)
        self.num_moves += 1

    def goal_reached(self):
        return self.board.goal_reached(self.position)


class Game:
    def __init__(self, board):
        self.board = board

    def play(self):
        player = Player(self.board)
        while not player.goal_reached():
            player.make_move()
        return player.num_moves


def experiment(num_games, seed, board):
    random.seed(seed)
    durations = []
    for _ in range(num_games):
        game = Game(board)
        num_moves = game.play()
        durations.append(num_moves)
    return durations


if __name__ == "__main__":

    board = Board()

    durations = experiment(1000, 1710, board)

    print(f'Shortest game duration: {min(durations):4d}')
    print(f'Mean game duration    : {np.mean(durations):6.1f} ± {np.std(durations):.1f}')
    print(f'Longest game duration : {max(durations):4d}')

    hv, hb = np.histogram(durations, bins=np.arange(0, max(durations)))
    plt.figure(figsize=(8, 3))
    plt.step(hb[:-1], hv)
    plt.show()
